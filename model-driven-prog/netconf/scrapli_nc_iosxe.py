from scrapli_netconf.driver import NetconfDriver
from device_info import csr1

device = {
    "host": csr1.get("address"),
    "auth_username": csr1.get("username"),
    "auth_password": csr1.get("password"),
    "auth_strict_key": False,
    "port": 830
}

conn = NetconfDriver(**device)
conn.open()
response = conn.get_config(source="running")

print(response.result)