from ncclient import manager
import sys
from device_info import nxos_device


def main():
    with manager.connect(
        host=nxos_device["address"],
        port=nxos_device["netconf_port"],
        username=nxos_device["username"],
        password=nxos_device["password"],
        hostkey_verify=False,
    ) as m:
        print(f"****** List of capabilities for {nxos_device['address']}******")
        for capability in m.server_capabilities:
            print(capability.split("?")[0])


if __name__ == "__main__":
    sys.exit(main())
