module: Cisco-NX-OS-device
  +--rw System
     +--rw ipv4-items
        +--rw name?         naming_Name256
        +--rw adminSt?      nw_AdminSt
        +--ro operSt?       nw_EntOperSt
        +--rw inst-items
           +--rw sourceRoute?                    nw_AdminSt
           +--rw accessListMatchLocal?           nw_AdminSt
           +--rw hardwareEcmpHashOffsetValue?    uint16
           +--rw hardwareEcmpHashOffsetConcat?   nw_AdminSt
           +--rw hardwareEcmpHashPolynomial?     ip_HardwareEcmpHashPolynomialT
           +--rw loggingLevel?                   arp_LoggingLevel
           +--rw name?                           naming_Name256
           +--rw adminSt?                        nw_AdminSt
           +--rw ctrl?                           nw_InstCtrl
           +--rw dom-items
           |  +--rw Dom-list* [name]
           |     +--rw autoDiscard?           nw_AdminSt
           |     +--rw ipIcmpErrorsSrcIntf?   nw_IfId
           |     +--rw name                   naming_Name256
           |     +--rw rtstaticbfd-items
           |     |  +--rw RtStaticBfd-list* [intf nexthop]
           |     |     +--rw intf       nw_IfId
           |     |     +--rw nexthop    address_Ip
           |     +--rw if-items
           |     |  +--rw If-list* [id]
           |     |     +--rw directedBroadcast?     enumeration
           |     |     +--rw acl?                   string
           |     |     +--rw forward?               nw_AdminSt
           |     |     +--rw unnumbered?            nw_IfId
           |     |     +--rw urpf?                  ip_UrpfT
           |     |     +--rw dropGlean?             nw_AdminSt
           |     |     +--rw id                     nw_IfId
           |     |     +--ro operSt?                ip_OperSt
           |     |     +--ro operStQual?            ip_UnnumberedOperStQual
           |     |     +--rw addr-items
           |     |     |  +--rw Addr-list* [addr]
           |     |     |     +--rw addr                   address_Ip
           |     |     |     +--rw type?                  ip_AddrT
           |     |     |     +--rw ctrl?                  ip_AddrControl
           |     |     |     +--rw vpcPeer?               address_Ip
           |     |     |     +--rw pref?                  ip_Preference
           |     |     |     +--rw tag?                   rt_Tag
           |     |     |     +--ro operSt?                ip_OperSt
           |     |     |     +--ro operStQual?            ip_AddrOperStQual
           |     |     |     +--rw rsipAddr-items
           |     |     |     |  +--rw RsIpAddr-list* [tDn]
           |     |     |     |     +--rw tDn    reln_Dn
           |     |     |     +--rw rsrtDefIpAddr-items
           |     |     |     |  +--rw RsRtDefIpAddr-list* [tDn]
           |     |     |     |     +--rw tDn    reln_Dn
           |     |     |     +--rw rsaddrToIpDef-items
           |     |     |        +--rw RsAddrToIpDef-list* [tDn]
           |     |     |           +--rw tDn    reln_Dn
           |     |     +--rw dampening-items
           |     |     |  +--rw halfLifePeriod?      uint64
           |     |     |  +--rw reuseThreshold?      uint64
           |     |     |  +--rw suppressThreshold?   uint64
           |     |     |  +--rw maxSuppress?         uint64
           |     |     |  +--rw restartEnabled?      nw_AdminSt
           |     |     |  +--rw restartPenalty?      uint64
           |     |     +--rw dampeningdata-items
           |     |     |  +--ro flapCount?           uint64
           |     |     |  +--ro penalty?             uint64
           |     |     |  +--ro suppressed?          uint64
           |     |     |  +--ro reuseTime?           uint64
           |     |     |  +--ro halfLifePeriod?      uint64
           |     |     |  +--ro reuseThreshold?      uint64
           |     |     |  +--ro suppressThreshold?   uint64
           |     |     |  +--ro maxSuppress?         uint64
           |     |     |  +--ro maxPenalty?          uint64
           |     |     |  +--ro restartPenalty?      uint64
           |     |     +--rw trafficstat-items
           |     |     |  +--ro upktSent?        uint64
           |     |     |  +--ro upktRecv?        uint64
           |     |     |  +--ro upktFwd?         uint64
           |     |     |  +--ro upktOrig?        uint64
           |     |     |  +--ro upktConsumed?    uint64
           |     |     |  +--ro ubyteSent?       uint64
           |     |     |  +--ro ubyteRcv?        uint64
           |     |     |  +--ro ubyteFwd?        uint64
           |     |     |  +--ro ubyteOrig?       uint64
           |     |     |  +--ro ubyteConsumed?   uint64
           |     |     |  +--ro mpktSent?        uint64
           |     |     |  +--ro mpktRcv?         uint64
           |     |     |  +--ro mpktFwd?         uint64
           |     |     |  +--ro mpktOrig?        uint64
           |     |     |  +--ro mpktConsumed?    uint64
           |     |     |  +--ro mbyteSent?       uint64
           |     |     |  +--ro mbyteRcv?        uint64
           |     |     |  +--ro mbyteFwd?        uint64
           |     |     |  +--ro mbyteOrig?       uint64
           |     |     |  +--ro mbyteConsumed?   uint64
           |     |     |  +--ro bpktSent?        uint64
           |     |     |  +--ro bpktRcv?         uint64
           |     |     |  +--ro bpktFwd?         uint64
           |     |     |  +--ro bpktOrig?        uint64
           |     |     |  +--ro bpktConsumed?    uint64
           |     |     |  +--ro bbyteSent?       uint64
           |     |     |  +--ro bbyteRcv?        uint64
           |     |     |  +--ro bbyteFwd?        uint64
           |     |     |  +--ro bbyteOrig?       uint64
           |     |     |  +--ro bbyteConsumed?   uint64
           |     |     |  +--ro lpktSent?        uint64
           |     |     |  +--ro lpktRcv?         uint64
           |     |     |  +--ro lpktFwd?         uint64
           |     |     |  +--ro lpktOrig?        uint64
           |     |     |  +--ro lpktConsumed?    uint64
           |     |     |  +--ro lbyteSent?       uint64
           |     |     |  +--ro lbyteRcv?        uint64
           |     |     |  +--ro lbyteFwd?        uint64
           |     |     |  +--ro lbyteOrig?       uint64
           |     |     |  +--ro lbyteConsumed?   uint64
           |     |     +--rw stat-items
           |     |     |  +--ro iodValue?           uint64
           |     |     |  +--ro vrfNameOut?         string
           |     |     |  +--ro protoState?         string
           |     |     |  +--ro linkState?          string
           |     |     |  +--ro adminState?         string
           |     |     |  +--ro priAddr?            address_Ipv4
           |     |     |  +--ro priSubnet?          string
           |     |     |  +--ro priMasklen?         uint8
           |     |     |  +--ro priRoutePref?       uint8
           |     |     |  +--ro priTag?             uint32
           |     |     |  +--ro secAddr?            string
           |     |     |  +--ro secAddrSubnet?      string
           |     |     |  +--ro secAddrMasklen?     string
           |     |     |  +--ro secAddrRoutePref?   string
           |     |     |  +--ro secAddrTag?         string
           |     |     |  +--ro numAddr?            uint32
           |     |     |  +--ro vaddrClient?        string
           |     |     |  +--ro vaddrPrefix?        string
           |     |     |  +--ro vaddrSubnet?        string
           |     |     |  +--ro vaddrMasklen?       string
           |     |     |  +--ro numVaddr?           uint64
           |     |     |  +--ro unnumIntf?          string
           |     |     |  +--ro firstIod?           uint64
           |     |     |  +--ro unnumChildIntf?     string
           |     |     |  +--ro ipDisabled?         string
           |     |     |  +--ro bcastAddr?          address_Ipv4
           |     |     |  +--ro mAddr?              string
           |     |     |  +--ro numMaddr?           uint32
           |     |     |  +--ro mtu?                uint16
           |     |     |  +--ro pref?               uint8
           |     |     |  +--ro tag?                uint32
           |     |     |  +--ro proxyArp?           nw_AdminSt_IfStat_proxyArp
           |     |     |  +--ro localProxyArp?      nw_AdminSt_IfStat_localProxyArp
           |     |     |  +--ro mRouting?           nw_AdminSt_IfStat_mRouting
           |     |     |  +--ro icmpRedirect?       nw_AdminSt_IfStat_icmpRedirect
           |     |     |  +--ro directBcast?        nw_AdminSt_IfStat_directBcast
           |     |     |  +--ro ipUnreach?          nw_AdminSt_IfStat_ipUnreach
           |     |     |  +--ro portUnreach?        nw_AdminSt_IfStat_portUnreach
           |     |     |  +--ro urpfMode?           string
           |     |     |  +--ro ipLoadSharing?      string
           |     |     |  +--ro aclIn?              string
           |     |     |  +--ro aclOut?             string
           |     |     |  +--ro statsLastReset?     string
           |     |     +--rw rpol-items
           |     |     |  +--rw rmapname?   string
           |     |     |  +--rw name?       pol_ObjName
           |     |     |  +--rw descr?      naming_Descr
           |     |     +--rw rtvrfMbr-items
           |     |        +--rw tDn?   reln_Dn
           |     +--rw multicastrt-items
           |     |  +--rw MulticastRoute-list* [prefix]
           |     |     +--rw prefix      address_Ip
           |     |     +--rw nh-items
           |     |        +--rw MulticastNexthop-list* [nhIf nhAddr nhVrf]
           |     |           +--rw nhIf      nw_IfId
           |     |           +--rw nhAddr    address_Ip
           |     |           +--rw nhVrf     l3_VrfName
           |     |           +--rw pref?     uint8
           |     +--rw pstat-items
           |     |  +--ro contextName?      string
           |     |  +--ro contextId?        uint64
           |     |  +--ro baseTId?          uint64
           |     |  +--ro autoDisc?         nw_AdminSt_ProcessStat_autoDisc
           |     |  +--ro autoDiscAdd?      nw_AdminSt_ProcessStat_autoDiscAdd
           |     |  +--ro nullBcast?        nw_AdminSt_ProcessStat_nullBcast
           |     |  +--ro autoPuntBcast?    nw_AdminSt_ProcessStat_autoPuntBcast
           |     |  +--ro staticDisc?       nw_AdminSt_ProcessStat_staticDisc
           |     |  +--ro staticDefRoute?   uint64
           |     |  +--ro ipUnreach?        uint64
           |     |  +--ro entryIod?         string
           |     |  +--ro localAddr?        string
           |     +--rw rt-items
           |     |  +--rw Route-list* [prefix]
           |     |     +--rw name?                      pol_ObjName
           |     |     +--rw descr?                     naming_Descr
           |     |     +--rw prefix                     address_Ip
           |     |     +--rw pref?                      rt_Preference
           |     |     +--rw tag?                       rt_Tag
           |     |     +--rw pcTag?                     actrl_PcTag
           |     |     +--rw ctrl?                      ip_RtControl
           |     |     +--rw nh-items
           |     |     |  +--rw Nexthop-list* [nhIf nhAddr nhVrf]
           |     |     |     +--rw rtname?                        string
           |     |     |     +--rw tag?                           uint32
           |     |     |     +--rw pref?                          uint8
           |     |     |     +--rw object?                        uint32
           |     |     |     +--rw name?                          pol_ObjName
           |     |     |     +--rw descr?                         naming_Descr
           |     |     |     +--rw nhIf                           nw_IfId
           |     |     |     +--rw nhAddr                         address_Ip
           |     |     |     +--rw nhVrf                          l3_VrfName
           |     |     |     +--rw rwEncap?                       string
           |     |     |     +--ro flags?                         ip_NhFlags
           |     |     |     +--ro operSt?                        ip_OperSt
           |     |     |     +--rw rsnexthopToNexthopDef-items
           |     |     |     |  +--ro RsNexthopToNexthopDef-list* [tDn]
           |     |     |     |     +--ro tDn    reln_Dn
           |     |     |     +--rw rsnexthopToProtG-items
           |     |     |        +--ro RsNexthopToProtG-list* [tDn]
           |     |     |           +--ro tDn    reln_Dn
           |     |     +--rw nhs-items
           |     |     |  +--ro NexthopStub-list* [nhIf nhAddr nhVrf]
           |     |     |     +--ro name?                          pol_ObjName
           |     |     |     +--ro descr?                         naming_Descr
           |     |     |     +--ro nhIf                           nw_IfId
           |     |     |     +--ro nhAddr                         address_Ip
           |     |     |     +--ro nhVrf                          l3_VrfName
           |     |     |     +--ro rwEncap?                       string
           |     |     |     +--ro flags?                         ip_NhFlags
           |     |     |     +--ro operSt?                        ip_OperSt
           |     |     |     +--ro rsnexthopToNexthopDef-items
           |     |     |     |  +--ro RsNexthopToNexthopDef-list* [tDn]
           |     |     |     |     +--ro tDn    reln_Dn
           |     |     |     +--ro rsnexthopToProtG-items
           |     |     |        +--ro RsNexthopToProtG-list* [tDn]
           |     |     |           +--ro tDn    reln_Dn
           |     |     +--rw rsrouteToRouteDef-items
           |     |     |  +--ro RsRouteToRouteDef-list* [tDn]
           |     |     |     +--ro tDn    reln_Dn
           |     |     +--rw rsrouteToIfConn-items
           |     |        +--ro RsRouteToIfConn-list* [tDn]
           |     |           +--ro tDn    reln_Dn
           |     +--rw routestat-items
           |     |  +--ro contextName?     string
           |     |  +--ro contextId?       uint32
           |     |  +--ro prefixMask?      string
           |     |  +--ro nhPrefixMask?    string
           |     |  +--ro vrfInfo?         string
           |     |  +--ro intrInfo?        string
           |     |  +--ro uribStat?        string
           |     |  +--ro nhopUribStat?    string
           |     |  +--ro trackObjNum?     string
           |     |  +--ro trackObjState?   string
           |     +--rw ifbriefstats-items
           |        +--ro VrfIfBriefStats-list* [interfaceId]
           |           +--ro interfaceId    nw_IfId
           |           +--ro addr?          address_Ip
           |           +--ro adminSt?       nw_AdminSt
           |           +--ro protSt?        nw_AdminSt
           |           +--ro linkSt?        nw_AdminSt
           |           +--ro secAddrList?   string
           +--rw pps-items
           |  +--rw IpPacketsPerSecond-list* [ipPpsType]
           |     +--rw ipPpsType                    ipv4_IpPpsTypes
           |     +--rw ipPpsThresholdSize?          uint32
           |     +--rw ipPpsThresholdLogInterval?   uint32
           +--rw client-items
           |  +--ro Client-list* [clientName clientProtocol]
           |     +--ro clientName              string
           |     +--ro clientUuid?             int32
           |     +--ro clientPid?              int32
           |     +--ro clientExtPid?           uint32
           |     +--ro clientProtocol          uint8
           |     +--ro clientIndex?            uint16
           |     +--ro clientContextId?        uint32
           |     +--ro clientMtsSap?           ipv4_ClientMtsSapType
           |     +--ro clientFlag?             ipv4_ClientFlagType
           |     +--ro clientDataMsgSuccess?   uint64
           |     +--ro clientDataMsgFail?      uint64
           |     +--ro clientRcvFunName?       string
           +--rw iploadsharing-items
           |  +--rw loadShareMode?     ip_LoadShareFormat
           |  +--rw universalID?       uint32
           |  +--rw greOuterHash?      nw_AdminSt
           |  +--rw udfOffset?         uint16
           |  +--rw udfLength?         uint16
           |  +--rw concatenation?     nw_AdminSt
           |  +--rw rotate?            uint16
           |  +--rw ecmpLoadSharing?   nw_AdminSt
           +--rw loadstat-items
           |  +--ro univerIdRanSeed?     uint32
           |  +--ro ipLoadshareOption?   string
           |  +--ro hashFlag?            nw_AdminSt
           |  +--ro concat?              nw_AdminSt
           |  +--ro rotate?              uint16
           +--rw iptrafficstat-items
           |  +--ro received?             uint32
           |  +--ro sent?                 uint32
           |  +--ro consumed?             uint32
           |  +--ro fwdUcast?             uint32
           |  +--ro fwdMcast?             uint32
           |  +--ro fwdLabel?             uint32
           |  +--ro ingressMcecFwdPkts?   uint32
           |  +--ro optsEnd?              uint32
           |  +--ro optsNop?              uint32
           |  +--ro optsBsec?             uint32
           |  +--ro optsLsrr?             uint32
           |  +--ro optsTimestamp?        uint32
           |  +--ro optsEsec?             uint32
           |  +--ro optsRecordRoute?      uint32
           |  +--ro optsStrsrcRoute?      uint32
           |  +--ro optsAlert?            uint32
           |  +--ro optsOther?            uint32
           |  +--ro frag?                 uint32
           |  +--ro fragmented?           uint32
           |  +--ro outFrag?              uint32
           |  +--ro fragDrop?             uint32
           |  +--ro cantFrag?             uint32
           |  +--ro reasm?                uint32
           |  +--ro fragTO?               uint32
           +--rw iptrafficerrstat-items
           |  +--ro badCsum?                uint32
           |  +--ro tooSmall?               uint32
           |  +--ro badVer?                 uint32
           |  +--ro badHLen?                uint32
           |  +--ro badLen?                 uint32
           |  +--ro badDest?                uint32
           |  +--ro badTTL?                 uint32
           |  +--ro cantFwd?                uint32
           |  +--ro outDrop?                uint32
           |  +--ro badEncap?               uint32
           |  +--ro noRoute?                uint32
           |  +--ro noProto?                uint32
           |  +--ro badOptions?             uint32
           |  +--ro vinciMigPkts?           uint32
           |  +--ro snoopSuccess?           uint32
           |  +--ro sviIodDown?             uint32
           |  +--ro restartPktDrop?         uint32
           |  +--ro mbufErrCount?           uint32
           |  +--ro badCntxtIdDrop?         uint32
           |  +--ro rpfDrop?                uint32
           |  +--ro badGwMacDrop?           uint32
           |  +--ro ipsOptionFail?          uint32
           |  +--ro natInDrop?              uint32
           |  +--ro natOutDrop?             uint32
           |  +--ro ipsMfwdFail?            uint32
           |  +--ro ipsLispDrop?            uint32
           |  +--ro ipsLispDecapDrop?       uint32
           |  +--ro ipsLispEncapDrop?       uint32
           |  +--ro ipsLispEncap?           uint32
           |  +--ro ipsMfwdCopyDrop?        uint32
           |  +--ro ipsRaReassDrop?         uint32
           |  +--ro ipsIcmpRedirProcDrop?   uint32
           |  +--ro ipsIfmgrInitFail?       uint32
           |  +--ro ipsInvalidFilter?       uint32
           |  +--ro ipsInvalidL2msg?        uint32
           |  +--ro aclIngressDrop?         uint32
           |  +--ro aclEgressDrop?          uint32
           |  +--ro aclDirBcastDrop?        uint32
           +--rw icmptrafficstat-items
           |  +--ro txRedir?                  uint32
           |  +--ro txUnreach?                uint32
           |  +--ro txEchoReq?                uint32
           |  +--ro txEchoReply?              uint32
           |  +--ro txMaskReq?                uint32
           |  +--ro txMaskReply?              uint32
           |  +--ro txInfoReq?                uint32
           |  +--ro txInfoReply?              uint32
           |  +--ro txParamProb?              uint32
           |  +--ro txSrcQuench?              uint32
           |  +--ro txTstampReq?              uint32
           |  +--ro txTstampReply?            uint32
           |  +--ro txTstampXceed?            uint32
           |  +--ro txRtrSolicit?             uint32
           |  +--ro txRtrAdvert?              uint32
           |  +--ro outBadLen?                uint32
           |  +--ro outEncapFail?             uint32
           |  +--ro outXmitFail?              uint32
           |  +--ro icmpOrigReq?              uint32
           |  +--ro redirOrigReq?             uint32
           |  +--ro outErr?                   uint32
           |  +--ro oldShortIp?               uint32
           |  +--ro oldIcmp?                  uint32
           |  +--ro errorDrop?                uint32
           |  +--ro rxRedir?                  uint32
           |  +--ro rxUnreach?                uint32
           |  +--ro rxEchoReq?                uint32
           |  +--ro rxEchoReply?              uint32
           |  +--ro rxMaskReq?                uint32
           |  +--ro rxMaskReply?              uint32
           |  +--ro rxInfoReq?                uint32
           |  +--ro rxInfoReply?              uint32
           |  +--ro rxParamProb?              uint32
           |  +--ro rxSrcQuench?              uint32
           |  +--ro rxTstampReq?              uint32
           |  +--ro rxTstampReply?            uint32
           |  +--ro rxTimeXceed?              uint32
           |  +--ro rxRtrSolicit?             uint32
           |  +--ro rxRtrAdvert?              uint32
           |  +--ro rxFormatError?            uint32
           |  +--ro rxCsumError?              uint32
           |  +--ro icmpLispProcessed?        uint32
           |  +--ro icmpNoClients?            uint32
           |  +--ro icmpConsumed?             uint32
           |  +--ro icmpReplies?              uint32
           |  +--ro icmpReplyDropInvldAddr?   uint32
           |  +--ro icmpReplyDropInactAddr?   uint32
           |  +--ro statsLastReset?           string
           +--rw rfc4293trafficstat-items
              +--ro inrcv?            uint32
              +--ro inoctet?          uint32
              +--ro inhdrerr?         uint32
              +--ro innoroutes?       uint32
              +--ro inaddrerr?        uint32
              +--ro innoproto?        uint32
              +--ro intruncated?      uint32
              +--ro inforwdgrams?     uint32
              +--ro reasmreqds?       uint32
              +--ro reasmoks?         uint32
              +--ro reasmfails?       uint32
              +--ro indiscards?       uint32
              +--ro indelivers?       uint32
              +--ro inMcastPkts?      uint32
              +--ro inMcastBytes?     uint32
              +--ro inBcastPkts?      uint32
              +--ro outRequest?       uint32
              +--ro outNoRoutes?      uint32
              +--ro outForwdGrams?    uint32
              +--ro outDiscards?      uint32
              +--ro outFragReqds?     uint32
              +--ro outFragOks?       uint32
              +--ro outFragFails?     uint32
              +--ro outFragCreates?   uint32
              +--ro outTransmits?     uint32
              +--ro outBytes?         uint32
              +--ro outMcastPkts?     uint32
              +--ro outMcastBytes?    uint32
              +--ro outBcastPkts?     uint32
              +--ro outBcastBytes?    uint32
