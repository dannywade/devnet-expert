from ncclient import manager
import sys
from device_info import nxos_device

asn_filter = """
 <System xmlns="http://cisco.com/ns/yang/cisco-nx-os-device">
     <bgp-items>
         <inst-items>
             <asn/>
         </inst-items>
     </bgp-items>
 </System>
"""

bgp_rtrid_filter = """
 <System xmlns="http://cisco.com/ns/yang/cisco-nx-os-device">
     <bgp-items>
         <inst-items>
             <dom-items>
                 <Dom-list>
                     <rtrId/>
                 </Dom-list>
             </dom-items>
         </inst-items>
     </bgp-items>
 </System>
 """

def main():
    with manager.connect(
        host=nxos_device["address"],
        port=nxos_device["netconf_port"],
        username=nxos_device["username"],
        password=nxos_device["password"],
        hostkey_verify=False,
        device_params={'name':'nexus'}
    ) as m:
        print(f"Getting BGP ASN from device...")
        netconf_response = m.get("subtree", asn_filter)
        print(netconf_response)

if __name__ == "__main__":
    sys.exit(main())