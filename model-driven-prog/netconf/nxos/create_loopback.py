from ncclient import manager
import sys
from device_info import nxos_device

loopback88 = {"id": "88", "ip": "10.88.88.88/24"}

loopback_add = """
<config>
    <System xmlns="http://cisco.com/ns/yang/cisco-nx-os-device">
        <intf-items>
            <lb-items>
                <LbRtdIf-list>
                    <id>lo{}</id>
                    <adminSt>up</adminSt>
                    <descr>Added via NETCONF - DVD</descr>
                </LbRtdIf-list>
            </lb-items>
        </intf-items>
    </System>
</config>
""".format(loopback88["id"])

loopback_ip_add = """
 <config>
 <System xmlns="http://cisco.com/ns/yang/cisco-nx-os-device">
 <ipv4-items>
     <inst-items>
         <dom-items>
             <Dom-list>
                 <name>default</name>
                 <if-items>
                     <If-list>
                         <id>lo{}</id>
                         <addr-items>
                             <Addr-list>
                                 <addr>{}</addr>
                             </Addr-list>
                         </addr-items>
                     </If-list>
                 </if-items>
             </Dom-list>
         </dom-items>
     </inst-items>
 </ipv4-items>
 </System>
 </config>""".format(loopback88["id"], loopback88["ip"])

def main():
    with manager.connect(
        host=nxos_device["address"],
        port=nxos_device["netconf_port"],
        username=nxos_device["username"],
        password=nxos_device["password"],
        hostkey_verify=False,
        device_params={'name':'nexus'}
    ) as m:
        print(f"Adding Loopback{loopback88['id']} to device...")
        m.edit_config(loopback_add, target="running")
        if m.ok:
            print("Added Loopback interface successfully!")
        else:
            print("Operation failed!")
        m.edit_config(loopback_ip_add, target="running")
        if m.ok:
            print("Added Loopback IP successfully!")
        else:
            print("Operation failed!")

if __name__ == "__main__":
    sys.exit(main())