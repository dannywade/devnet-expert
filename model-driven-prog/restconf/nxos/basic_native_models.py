from requests import get
from requests.auth import HTTPBasicAuth
from device_info import nxsw1
import urllib3
import json

urllib3.disable_warnings()


### FULL CONFIG ###
# Get running config of device
device_url = f"https://{nxsw1['address']}/restconf/data/Cisco-NX-OS-device:System"
dev_headers = {
    "Content-Type": "application/yang-data+xml",
    "Accept": "application/yang-data+xml",
}

full_config = get(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(nxsw1['username'], nxsw1['password']), verify=False)

print(full_config.text)