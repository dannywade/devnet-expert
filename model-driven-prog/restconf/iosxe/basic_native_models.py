from pprint import pprint
from requests import get
from requests.auth import HTTPBasicAuth
from device_info import csr1
import urllib3
import json

urllib3.disable_warnings()

### FULL CONFIG ###
# Get running config of device
device_url = f"https://{csr1['address']}/restconf/data/Cisco-IOS-XE-native:native"
dev_headers = {
    "Content-Type": "application/yang-data+json",
    "Accept": "application/yang-data+json",
}

full_config = get(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr1['username'], csr1['password']), verify=False).json()

# Convert Python dict to JSON string
# pprint(full_config)

### INTERFACE DETAILS ###
# Get specific interface details
device_url = f"https://{csr1['address']}/restconf/data/Cisco-IOS-XE-native:native/interface/GigabitEthernet=1/ip"

interface_details = get(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr1['username'], csr1['password']), verify=False).json()

# Convert Python dict to JSON string
# pprint(interface_details)


### ONLY GIG INTERFACES WITH AN IP ADDRESS
# Get specific interface details
device_url = f"https://{csr1['address']}/restconf/data/Cisco-IOS-XE-native:native/interface//GigabitEthernet/ip/address/primary"

interface_details = get(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr1['username'], csr1['password']), verify=False).json()

# Convert Python dict to JSON string
pprint(interface_details)
