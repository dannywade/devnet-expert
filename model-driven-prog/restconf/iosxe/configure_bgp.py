from requests import put
from requests.auth import HTTPBasicAuth
from device_info import csr2
import urllib3
import json

urllib3.disable_warnings()

device_url = f"https://{csr2['address']}/restconf/data/Cisco-IOS-XE-native:native/router"
dev_headers = {
    "Content-Type": "application/yang-data+json",
    "Accept": "application/yang-data+json",
}

dev_payload = {
    "Cisco-IOS-XE-native:router": {
        "Cisco-IOS-XE-bgp:bgp": [
            {
                "id": 65002,
                "bgp": {
                    "router-id": {
                        "ip-id": "2.2.2.2"
                    }
                },
                "neighbor": [
                    {
                        "id": "10.10.254.1",
                        "remote-as": 65001,
                        "log-neighbor-changes": {}
                    }
                ]
            }
        ]
    }
}

dev_payload = json.dumps(dev_payload)

configure_bgp = put(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr2['username'], csr2['password']), data=dev_payload, verify=False)

print(configure_bgp.text)

if configure_bgp.status_code == 200 or 201:
    print("BGP process has been successfully created!")
elif configure_bgp.status_code == 409:
    print("There is a BGP process already configured on the device.")
