from pprint import pprint
from requests import get, patch, post
from requests.auth import HTTPBasicAuth
from device_info import csr1
import urllib3
import json

urllib3.disable_warnings()

device_url = f"https://{csr1['address']}/restconf/data/ietf-interfaces:interfaces"
dev_headers = {
    "Content-Type": "application/yang-data+json",
    "Accept": "application/yang-data+json",
}

dev_payload = {
"interface": [
    {
        "name": "Loopback100",
        "description": "Added via RESTCONF",
        "type": "iana-if-type:softwareLoopback",
        "enabled": "true",
        "ietf-ip:ipv4": {
            "address":[
                {
                    "ip": "10.100.1.100",
                    "netmask": "255.255.255.255"
                }

            ]
        }

    }
]
}

dev_payload = json.dumps(dev_payload)

create_loopback = post(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr1['username'], csr1['password']), data=dev_payload, verify=False)

if create_loopback.status_code == 201:
    print("Loopback interface has been successfully created!")
elif create_loopback.status_code == 409:
    print("There is an identical Loopback interface already configured on the device.")
