import json
from requests import put
from requests.auth import HTTPBasicAuth
from device_info import csr2
import urllib3

urllib3.disable_warnings()

device_url = f"https://{csr2['address']}/restconf/data/Cisco-IOS-XE-native:native/interface/GigabitEthernet=2"
dev_headers = {
    "Content-Type": "application/yang-data+json",
    "Accept": "application/yang-data+json",
}

dev_payload = {
    "Cisco-IOS-XE-native:GigabitEthernet": {
        "name": "2",
        "ip": {
            "address": {
                "primary": {
                    "address": "10.10.254.2",
                    "mask": "255.255.255.0"
                }
            }
        }
    }
}

dev_payload = json.dumps(dev_payload)

configure_interface = put(url=device_url, headers=dev_headers, data=dev_payload, auth=HTTPBasicAuth(csr2['username'], csr2['password']), verify=False)

print(configure_interface.text)

if configure_interface.status_code == 200 or 201:
    print("The interface has been successfully updated!")
elif configure_interface.status_code == 409:
    print("There was a conflict when updating the interface.")