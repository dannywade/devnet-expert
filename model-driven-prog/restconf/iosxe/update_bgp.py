from requests import put
from requests.auth import HTTPBasicAuth
from device_info import csr1
import urllib3
import json

urllib3.disable_warnings()

device_url = f"https://{csr1['address']}/restconf/data/Cisco-IOS-XE-native:native/router"
dev_headers = {
    "Content-Type": "application/yang-data+json",
    "Accept": "application/yang-data+json",
}

dev_payload = {
    "Cisco-IOS-XE-native:router": {
        "Cisco-IOS-XE-bgp:bgp": [
            {
                "id": 65001,
                "bgp": {
                    "router-id": {
                        "ip-id": "1.1.1.1"
                    }
                },
                "neighbor": [
                    {
                        "id": "10.10.254.2",
                        "remote-as": 65002,
                        "log-neighbor-changes": {}
                    }
                ]
            }
        ]
    }
}

dev_payload = json.dumps(dev_payload)

# PUT - replaced entire BGP config with payload
# PATCH - merged payload with existing BGP config
configure_bgp = put(url=device_url, headers=dev_headers, auth=HTTPBasicAuth(csr1['username'], csr1['password']), data=dev_payload, verify=False)

print(configure_bgp.text)

if configure_bgp.status_code == 200 or 201:
    print("BGP process has been successfully updated!")
elif configure_bgp.status_code == 409:
    print("There was a conflict while updating the BGP configuration.")
