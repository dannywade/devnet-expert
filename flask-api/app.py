from flask import Flask, jsonify, request
from api_v0 import api_blueprint
import os

app = Flask(__name__)

app.register_blueprint(api_blueprint)

DOCKER_NAME = os.getenv("WEB_NAME", "No name")

# Testing error handling if user tries to use API v1
@app.errorhandler(404)
@app.errorhandler(405)
def _handle_api_error(ex):
    if request.path.startswith("/api/v1"):
        return jsonify(error=str(ex)), ex.code
    else:
        return ex


@app.route("/")
def index():
    return f"Welcome {DOCKER_NAME}!"


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
