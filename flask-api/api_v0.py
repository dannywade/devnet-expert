from flask import Blueprint, jsonify

api_blueprint = Blueprint("api", __name__, url_prefix="/api/v0")


@api_blueprint.route("/")
def api_root():
    data = "{'api': 'says hello'}"
    return jsonify(data)
