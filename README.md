# devnet-expert

Repo to store apps created for Cisco DevNet Expert lab studies.

## Terraform

For each Terraform example, you must create a .tfvars file to define the variables mentioned in each of the configuration files (per example directory). If not specificed, you'll be prompted to enter values for each variable when running `terraform plan` or `terraform apply`.

## Model-Driven Prog

For now, you'll need to create a `device_info.py` file in each sub-directory under the `model-driven-prof` directory to use as an inventory file. I've included an example file in the `model-driven-prof` directory (`device_info_example.py`). In the future, I plan to centralize a single inventory file to avoid duplication across multiple directories.
