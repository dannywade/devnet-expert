variable "instance_name" {
  description = "value of the name tag for EC2 instances"
  type = string
  default = "ExampleAppServerInstance2"
}