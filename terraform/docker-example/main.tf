terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}


provider "docker" {}

resource "docker_network" "new-net" {
  name = "my_dock_net"
  ipam_config {
    subnet   = "10.100.1.0/24"
    ip_range = "10.100.1.0/24"
    gateway  = "10.100.1.1"
  }
}



resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = false
}

resource "docker_container" "nginx_container" {
  image = docker_image.nginx.name
  name  = "MyWebServer"
  networks_advanced {
    name         = docker_network.new-net.name
    ipv4_address = "10.100.1.2"
  }
  ports {
      internal = 80
      external = 8080 # random port is provided (>= 32768) if not specified
  }
}