variable "username" {
  type = string
}

variable "password" {
  sensitive = true
}

variable "url" {
  type = string
}