terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

provider "aci" {
  username = "var.username"
  password = "var.password"
  url      = "var.url"
  insecure = false
}

# Provision Tenants
resource "aci_tenant" "terraform_tenant" {
  name        = "terraform_tenant"
  description = "Tenant created by Terraform" # optional
}

resource "aci_tenant" "tf_dan_tenant" {
  name        = "tf_dan"
  description = "Tenant created by Terraform for Dan" # optional
}

# VRF
resource "aci_vrf" "Blue_VRF" {
  name      = "Blue_VRF"
  tenant_dn = aci_tenant.terraform_tenant.id
}

# Bridge Domain (BD)
resource "aci_bridge_domain" "terraform_bd" {
  name      = "terraform_bd"
  tenant_dn = aci_tenant.terraform_tenant.id
}

# Subnet
resource "aci_subnet" "terraform_bd_subnet" {
  parent_dn   = aci_bridge_domain.terraform_bd.id
  description = "Subnet created by Terraform"
  ip          = "10.10.1.1/24" # Default gateway of subnet
  scope       = ["public"]
}