# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/josenk/esxi" {
  version = "1.10.0"
  hashes = [
    "h1:Rom/OvHmhXeGV1zahDtycB9b8rEd1Qt66wOcYjgbRtM=",
    "zh:0f0b464f6a20ccff574445aee115487c60ebed017bde9a3d21f67bd395a2734d",
    "zh:0f1dc91cdd4cbc6b98d26fb497cd73dfe2a52911b678fc53608e3d7c9a85fc5e",
    "zh:11cb964cec7cd332561eb212b33cce001001756aff8b554055c6584fb07d280c",
    "zh:49c3896d3e4408f3663d18518c14eaab7e56153da36ae98537ed46586556412e",
    "zh:50e408def6ec12e8a12084b101cdddde56a094db885b5ac1cc91685918a11c04",
    "zh:58e9221528a1ec3d5e11b119a810df676730b594d91222f14cfbed91e9fbd788",
    "zh:72e81a5112f0b7a791f1698d71dba40a18c909b1efa0fd87063bd379e57fa982",
    "zh:a1fd3b71e2f4d2b0500e322679a965b29de196bca4634ca7f3d7fab1caef09df",
    "zh:ba76d6ac3119d333399d9994c3833ac177049d250f9da4b3d83b3c1c1c8661b6",
    "zh:c0f8d0a07f3aea8b2ba740f37a5b705906d3f9ef7c44365bc60d017fe11a2510",
    "zh:c10ebd2b16a272d3cd0937d2865939ea268b9bac351a2003040d5d36da62e898",
    "zh:f9fc76385f2a2965a93410530f22108803600ff590b3afe87f3d134c039b5a37",
  ]
}
