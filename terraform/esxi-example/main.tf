terraform {
  required_providers {
    esxi = {
      source  = "josenk/esxi"
    }
  }
}

provider "esxi" {
  esxi_hostname = var.esxi_hostname
  esxi_hostport = var.esxi_hostport
  esxi_hostssl  = var.esxi_hostssl
  esxi_username = var.esxi_username
  esxi_password = var.esxi_password
}

resource "esxi_guest" "vmtest1" {
  guest_name = "vmtest1"
  disk_store = "datastore1"

  memsize            = "4096"
  numvcpus           = "1"
  power              = "on"
  resource_pool_name = "/"

  network_interfaces {
    virtual_network = "VM Network"
    mac_address     = "00:11:22:33:44:55"
    nic_type        = "vmxnet3"
  }
}